<?php

namespace HotWire\ORM;

abstract class AbstractRepository
{
    public function find($id, $entity = null)
    {
        $data=$this->findBy(array('id'=>$id), $entity);

        return current($data);
    }

    public function findAll()
    {
        return $this->findBy(array());
    }

    public function findBy(array $fields, $entityType = null)
    {
        $sql=SQLGenerator::select($this->getEntityName($entityType), $fields);
        $entities=array();
        foreach (DataAccess::select($sql, $fields) as $data) {
            $entities[]=$this->mapData($data, $entityType);
        }

        return $entities;
    }

    public function findOneBy(array $fields, $entityType=null)
    {
        $sql=SQLGenerator::select($this->getEntityName($entityType), $fields);
        $entities=array();
        $data=DataAccess::select($sql, $fields);

        return $this->mapData(current($data),$entityType);
    }

    public function getEntityName($entity=null)
    {
        $items=explode('::', $entity ? $entity : $this->getEntityType());

        return end($items);
    }

    public function getEntity($entityType=null)
    {
        $type=$this->getEntityType();
        if ($entityType) {
            $type=preg_replace('/::[A-Za-z]+/', "::{$entityType}", $type);
        }
        $entity=str_replace('::', '\\Entity\\', $type);
        $entity=str_replace(':', '\\', $entity);

        $reflectionClass=new \ReflectionClass($entity);

        return $reflectionClass->newInstance();
    }

    private function mapData(\stdClass $data, $entityType=null)
    {
        $entity=$this->getEntity($entityType);
        foreach ($data as $key => $value) {
            $this->setProperty($key, $value, $entity);
        }

        return $entity;
    }

    public function setProperty($property, $value, $object)
    {
        $reflectionClass=new \ReflectionClass($object);
        if ($reflectionClass->hasProperty($property)) {
            if ($propertInfo=$reflectionClass->getProperty($property)) {
                $propertInfo->setAccessible(true);
                $propertInfo->setValue($object, $value);
            }
        } else {
            $relatedProperty=str_replace('_id', null, $property);
            if ($associatedEntity=$this->find($value, ucfirst($relatedProperty))) {
                $this->setProperty($relatedProperty, $associatedEntity, $object);
            }
        }
    }

    abstract public function getEntityType();
}
