<?php

namespace HotWire\ORM;

abstract class Migration
{
    abstract public function up();
    abstract public function down();
    abstract public function seed();
}
