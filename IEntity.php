<?php

namespace HotWire\ORM;

interface IEntity
{
    public function getId();
}
