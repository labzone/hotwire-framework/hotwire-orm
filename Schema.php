<?php

namespace HotWire\ORM;

class Schema
{

    const int='int';
    const string='varchar';
    const datetime='datetime';
    const tinyint='tinyint';

    public static function id($name, $length=11)
    {
        $column=new Column();
        $column->setName($name)
               ->setType(self::int)
               ->setLength(11)
               ->setConstraints('not null primary key AUTO_INCREMENT');

        return $column;
    }

    public static function string($name, $length=50, $constraints = null)
    {
        $column=new Column();
        $column->setName($name)
               ->setLength($length)
               ->setConstraints($constraints)
               ->setType(self::string);

        return $column;
    }

    public static function datetime($name, $constraints = null)
    {
        $column=new Column();
        $column->setName($name)
               ->setType(self::datetime)
               ->setConstraints($constraints);

        return $column;
    }

    public static function tinyint($name, $length = 1, $constraints = null)
    {
        $column=new Column();
        $column->setName($name)
               ->setType(self::tinyint)
               ->setConstraints($constraints)
               ->setLength(1);

        return $column;
    }

    public static function bool($name, $constraints = null)
    {
        return self::tinyint($name,1,$constraints);
    }

    public static function foreign($name, $class, $constraints = null)
    {
        $class=str_replace('::', '\\Entity\\', $class);
        $class=str_replace(':', '\\', $class);
        $reflectionClass=new \ReflectionClass($class);
        $name=strtolower($reflectionClass->getShortName());
        $column=new Column();
        $column->setName("{$name}_id")
               ->setType(self::int)
               ->setConstraints($constraints)
               ->setReference("{$name}(id)");

        return $column;
    }
}
