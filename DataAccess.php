<?php

namespace HotWire\ORM;

class DataAccess
{
    private static $handler;

    public static function createTable(EntityProperties $entityProperties)
    {
        try {
            self::exec(SQLGenerator::create($entityProperties));
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function dropTable(EntityProperties $entityProperties)
    {
        self::exec(SQLGenerator::drop($entityProperties));
    }

    public static function insert(EntityProperties $entityProperties)
    {
        DatabaseHandler::getInstance()->query(SQLGenerator::insert($entityProperties));
    }

    public static function update(EntityProperties $entityProperties)
    {
        DatabaseHandler::getInstance()->query(SQLGenerator::update($entityProperties));
    }

    public static function remove(EntityProperties $entityProperties)
    {
        DatabaseHandler::getInstance()->query(SQLGenerator::delete($entityProperties));
    }

    public static function exec($sql)
    {
        DatabaseHandler::getInstance()->exec($sql);
    }

    public static function select($sql, array $params = null)
    {
        if ($params) {
            $select=DatabaseHandler::getInstance()->prepare($sql);
            foreach ($params as $key => $param) {
                $select->bindParam(":{$key}",$param);
            }
            $select->execute();
        } else {
            $select=DatabaseHandler::getInstance()->query($sql);
        }

        return $select->fetchAll(\PDO::FETCH_OBJ);
    }
}
