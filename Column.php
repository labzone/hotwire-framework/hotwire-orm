<?php

namespace HotWire\ORM;

class Column
{
    private $name;
    private $type;
    private $constraints;
    private $length;
    private $reference;
    private $value;

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of type.
     *
     * @return mixed
     */
    public function getType()
    {
        if ($this->length) {
            return "{$this->type}($this->length)";
        }

        return $this->type;
    }

    /**
     * Sets the value of type.
     *
     * @param mixed $type the type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets the value of constraints.
     *
     * @return mixed
     */
    public function getConstraints()
    {
        return $this->constraints;
    }

    /**
     * Sets the value of constraints.
     *
     * @param mixed $constraints the constraints
     *
     * @return self
     */
    public function setConstraints($constraints)
    {
        $this->constraints = $constraints;

        return $this;
    }

    /**
     * Gets the value of length.
     *
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Sets the value of length.
     *
     * @param mixed $length the length
     *
     * @return self
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    public function getReference()
    {
        return $this->reference;
    }

    public function setReference($reference)
    {
        $this->reference=", FOREIGN KEY ($this->name) REFERENCES {$reference}";

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value=$value;

        return $this;
    }
}
