<?php

namespace HotWire\ORM\DependencyInjection;

use HotWire\DependencyInjection\Extension;
use HotWire\ORM\Manager;
use HotWire\ORM\DataMapper;

class ORMExtension extends Extension
{
    public function load()
    {
        $this->container->register('entity.manager', new Manager())
                        ->register('data.mapper', new DataMapper());
    }
}
