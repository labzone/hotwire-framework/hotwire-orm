<?php

namespace HotWire\ORM;

use HotWire\Framework\AbstractApp;

class App extends AbstractApp
{
    public function boot()
    {
        parent::boot();
        $this->registerORM();
    }

    public function getName()
    {
        return 'HotWire:ORM';
    }

    public function registerORM()
    {
        return $this;
    }
}
