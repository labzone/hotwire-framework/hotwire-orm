<?php

namespace HotWire\ORM;

class EntityProperties
{
    private $name;
    private $columns=array();

    public function create()
    {
        DataAccess::createTable($this);
    }

    public function drop()
    {
        DataAccess::dropTable($this);
    }

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of columns.
     *
     * @return mixed
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Sets the value of columns.
     *
     * @param mixed $columns the columns
     *
     * @return self
     */
    public function setColumns(array $columns)
    {
        $this->columns = $columns;

        return $this;
    }

    public function addColumn($column)
    {
        $this->columns[]=$column;

        return $this;
    }

    public function get($columnName)
    {
        foreach ($this->columns as $key => $column) {
            if ($column->getName()==$columnName) {
                return $column;
            }
        }
    }
}
