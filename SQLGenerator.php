<?php

namespace HotWire\ORM;

class SQLGenerator
{
    public static function insert(EntityProperties $entityProperties)
    {
        $sql="INSERT INTO {$entityProperties->getName()} (";
        $fields=array();
        $values=array();
        foreach ($entityProperties->getColumns() as $key => $column) {
            if ($value=$column->getValue()) {
                $fields[]=$column->getName();
                if ($value instanceof IEntity) {
                    $values[]=$value->getId();
                } elseif ($value instanceof \DateTime) {
                    $values[]=$value->format('Y-m-d H:i:s');
                } else {
                    $values[]=$value;
                }
            }
        }
        $sql.= implode(',', $fields) . ") VALUES ( '";
        $sql.= implode("','", $values) . "') ";

        return $sql;
    }

    public static function select($tableName, array $values)
    {
        $sql="SELECT * FROM {$tableName} ";
        $where='WHERE ';
        $fields=null;
        $params=array();
        foreach ($values as $field => $value) {
            $fields.=$field;
            $where.="{$field}=:{$field}";
        }
        if ($fields) {
            $sql.=':where:';
            $sql=str_replace(':fields:', $fields, $sql);
            $sql=str_replace(':where:', $where, $sql);
        }

        return $sql;
    }

    public static function delete(EntityProperties $entityProperties)
    {
        $sql=null;
        if ($column=$entityProperties->get('id')) {
            $sql="DELETE FROM {$entityProperties->getName()} WHERE ID={$column->getValue()}";
        }

        return $sql;
    }

    public static function update(EntityProperties $entityProperties)
    {
        $sql="UPDATE {$entityProperties->getName()} SET ";
        $values=null;
        foreach ($entityProperties->getColumns() as $key => $column) {
            if ($column->getValue() && $column->getName() != 'id') {
                $values[]=" {$column->getName()} = '{$column->getValue()}'";
            }
        }
        $sql.= implode(',', $values);
        $sql.= " WHERE ID={$entityProperties->get('id')->getValue()}";

        return $sql;
    }

    public static function create(EntityProperties $entityProperties)
    {
        $sql="CREATE TABLE IF NOT EXISTS {$entityProperties->getName()} ( ";
        $columns=$entityProperties->getColumns();
        foreach ($columns as $key=>$column) {
            $sql.="{$column->getName()} {$column->getType()} {$column->getConstraints()} {$column->getReference()}";
            if (count($columns) != $key+1) {
                $sql.=', ';
            }
        }
        $sql.=');';

        return $sql;
    }

    public static function drop(EntityProperties $entityProperties)
    {
        return "DROP TABLE {$entityProperties->getName()}";
    }
}
