<?php

namespace HotWire\ORM;

use HotWire\Util\Annotations;

class DataMapper
{

    private $entity;
    private $entityProperties;

    public function __construct(IEntity $entity=null)
    {
        if ($entity) {
            $this->setEntity($entity);
        }
    }

    public function setEntity(IEntity $entity)
    {
        $this->entity=$entity;
        $this->entityProperties=new EntityProperties();
        $reflectionClass=new \ReflectionClass($entity);
        $this->entityProperties->setName($reflectionClass->getShortName());

        return $this;
    }

    public function map()
    {
        $class=Annotations::getClass($this->entity);
        $classInfo=new \ReflectionClass($class);
        foreach (Annotations::getMethods($this->entity) as $name=>$methodProperties) {
            if ($classInfo->hasMethod($methodProperties['method'])) {
                $methodInfo=$classInfo->getMethod($methodProperties['method']);
                $arguments=$methodProperties['arguments'];
                array_unshift($arguments, $name);
                if ($column=$methodInfo->invokeArgs(null, $arguments)) {
                    $getter="get".ucfirst($name);
                    $entityInfo=new \ReflectionClass($this->entity);
                    if ($entityInfo->hasMethod($getter)) {
                        $column->setValue($this->entity->$getter());
                    }
                }
                $this->entityProperties->addColumn($column);
            }
        }

        return $this->entityProperties;
    }
}
