<?php

namespace HotWire\ORM;

use HotWire\DependencyInjection\Container;

use PDO;

class DatabaseHandler
{
    private static $handler;

    private function __construct() { }

    private function __clone() { }

    public static function getInstance()
    {
        if (!self::$handler) {
            $config=null;
            if ($parameters=Container::getInstance()->get('parameters')) {
                $config=$parameters->getDatabaseConfig();
            }
            if ($config) {
                try {
                    $dsn="{$config->getEngine()}:host={$config->getHost()};dbname={$config->getName()}";
                    self::$handler=new PDO($dsn, $config->getUsername(), $config->getPassword());
                } catch (\PDOException $e) {
                    echo $e->getMessage();
                }
            }
        }

        return self::$handler;
    }
}
