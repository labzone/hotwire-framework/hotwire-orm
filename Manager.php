<?php

namespace HotWire\ORM;

class Manager
{
    private $mapper;

    public function __construct()
    {
        $this->mapper=new DataMapper();
    }

    public function persist(IEntity $entity)
    {
        $entityProperties=$this->mapper->setEntity($entity)->map();
        if ($entity->getId()) {
            $this->update($entityProperties);
        } else {
            $this->insert($entityProperties);
        }
    }

    private function update(EntityProperties $entityProperties)
    {
        DataAccess::update($entityProperties);
    }

    private function insert(EntityProperties $entityProperties)
    {
        DataAccess::insert($entityProperties);
    }

    public function remove(IEntity $entity)
    {
        $entityProperties=$this->mapper->setEntity($entity)->map();
        DataAccess::remove($entityProperties);
    }

    public function getRepository($repository)
    {
        $repositoryClass=str_replace('::', '\\Repository\\', $repository);
        $repositoryClass=str_replace(':', '\\', $repositoryClass);
        $reflectionClass=new \ReflectionClass($repositoryClass);

        return $reflectionClass->newInstance();
    }

    public function create()
    {
        $this->mapper->map()->create();
    }
}
